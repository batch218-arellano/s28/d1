// CRUD Operations
/*
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/


// [Section] Inserting Documents (CREATE)

/*

    Syntax:
        - db.collectionName.insertOne({object});
*/ 

// Insert One 

db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "87654321",
        email: "janedoe@mail.com"
    },
    courses: ["CSS", "Javascript", "Python"],
    department : "none"
});

// Insert Many 
db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "8700",
            email: "stephenhawking@mail.com"
        },
        courses: ["Python", "React", "PHP"],
        department : "none"
    },

    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87894561",
            email: "neilarmstrong@mail.com"
        },
        courses: ["React", "Laravel", "MongoDB"],
        department : "none"
    }
]);

// [Section] Finding Documents (READ)

/*  
    - db.collectionName.find();
    - db.collectionName.find({ field: value};

*/

db.users.find();

// ---------------

/* 

    db.collectionName.deteleOne({criteria/condition})
*/ 
// use this to delete a user or users. 
db.users.deleteOne({
    firstName: "Stephen"
});

db.users.deleteOne({
    firstName: "Neil"
});

db.users.find({
    name : "Stephen"
});

db. users. find({ lastName: "Armstrong", age: 82 }) ;

// ------ 


db.users.insert({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "00000000",
        email: "test@gmail.com"
    },
    courses: [],
    department: "none"
});

// [Section] Updating documents (UPDATE)

db.users.updateOne(
    { firstName : "Test"},
    {
        $set: {
            firstName: "Bill",
            lastName: "Gates",
            age: 65,
            contact :{
                 phone : "12345678",
                 email: "bill@mail.com"
            },
            courses: ["PHP","Laravel", "HTML"],
            department: "Operations",
            status: "active" 
        }
    
})

db.users.find({ firstName: "Bill" });

/* Updating multiple documents

 -Syntax:
 - db.collectionName.updateMany ({criteria}, {$set: {field: value}}) ;

*/

db.users.updateMany (
    {department: "none"},
    {
        $set: { department: "HR"}
    }
);

// Replace One
/*
     - Can be used if replacing the whole document is necessary.
     - If you would like to update the whole document.  
*/ 

db.users.replaceOne(
    {firstName: "Bill"}, //criteria
    { //field and values to replace
        firstName: "Billy",
        lastName: "Crawford",
        age: 30,
        contact :{
                 phone : "12345678",
                 email: "bill@mail.com"
            },
        courses: ["PHP","Laravel", "HTML"],
        department: "Operations",
        status: "active" 

    }
);

db.users.find();
//---- 

db.user.deleteOne({
    firstName : "Jane"
})

db.users.deleteMany({
    firstName : "Jane"
})




